import React, { useReducer, useContext } from 'react'
import { Alert, Platform } from 'react-native'
import { TodoContext } from './todoContext'
import { todoReducer } from './todoReducer'
import { ADD_TODO, FETCH_TODOS, REMOVE_TODO, UPDATE_TODO, SHOW_LOADER, 
         HIDE_LOADER, SHOW_ERROR, CLEAR_ERROR } from '../types'
import { ScreenContext } from '../screen/screenContext'

const ip_address = Platform.OS === 'android' ?
                  'http://10.0.2.2:8080/' : 'http://localhost:8080/'
// для тестирования на эмуляторах (разные ip-адреса localhost)

export const TodoState = ({ children }) => {
  const initialState = {
    //todos: [{ id: '1', name: 'Выучить React Native' }]
    todos: [],
    loading: false,
    error: null
  }

  const { changeScreen } = useContext(ScreenContext)
  const [state, dispatch] = useReducer(todoReducer, initialState)

  // const addTodo = name => dispatch({ type: ADD_TODO, name })
  const addTodo = async name => {
    const apiUrl = ip_address + 'post-item-by-name'
    const response = await fetch(
      apiUrl,
      {
        method: 'POST',
        headers: { 'Content-type': 'application/json' },
        body: JSON.stringify( {name} )
      }  
    )
    const data = await response.json()
    dispatch({ type: ADD_TODO, name: data.name, id: data.id })
  }
  

  const removeTodo = id => {
    const todo = state.todos.find(t => t.id === id)
    const apiUrl = ip_address + 'delete-item-by-id/'
    Alert.alert(
      'Delete todo',
      `Are you sure that you want to delete "${todo.name}"?`,
      [
        {
          text: 'Cancel',
          style: 'cancel'
        },
        {
          text: 'Delete',
          style: 'destructive',
          onPress: async () => {
            changeScreen(null)
            await fetch(apiUrl + `${id}`,
              { 
                method: 'DELETE',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify( {id} )
              }
            )
            dispatch({ type: REMOVE_TODO, id })
          }
        }
      ],
      { cancelable: false }
    )
  }


  const fetchTodos = async () => {
    showLoader()
    clearError() 
    const apiUrl = ip_address + 'get-items/'
    try {
      const response = await fetch(
        apiUrl,
        {
          method: 'GET',
          headers: { 'Content-Type': 'application/json' }
        }      
      )
      const todos = await response.json()
      // или такой вариант, который делает то же самое:
      // const data = await response.json()
      // const todos = Object.keys(data).map( key => {
      //   return ( { ...data[key], id: data[key].id } )
      // })

      // или как-то так (все зависит от ответа сервера):
      // const todos = Object.keys(data).map( key => ( { ...data[key], id: key } ) )

      dispatch( { type: FETCH_TODOS, todos } )
      // пример с задержкой загрузки 
      //setTimeout( () => dispatch( { type: FETCH_TODOS, todos } ), 5000 )
    } catch (e) {
        showError('Something went wrong...')
        console.log(e)
    } finally {
      hideLoader()
    } 
  }


  // const updateTodo = (id, name) => dispatch({ type: UPDATE_TODO, id, name })
  const updateTodo = async (id, name) => {
    clearError()
    const apiUrl = ip_address + 'put-item-name-by-id/'
    try {
      // 'PATCH' id
      await fetch(apiUrl,
      {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({id, name})
      })
      dispatch({ type: UPDATE_TODO, id, name })
    } catch (e) {
      showError('Something went wrong...')
      console.log(e)
    } 
  }


  const showLoader = () => dispatch({ type: SHOW_LOADER })
  const hideLoader = () => dispatch({ type: HIDE_LOADER })
  const showError = error => dispatch({ type: SHOW_ERROR, error })
  const clearError = () => dispatch({ type: CLEAR_ERROR })


  return (
    <TodoContext.Provider
      value={{
        todos: state.todos,
        loading: state.loading,
        error: state.error,
        addTodo,
        removeTodo,
        updateTodo,
        fetchTodos
      }}
    >
      {children}
    </TodoContext.Provider>
  )
}
